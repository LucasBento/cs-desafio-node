# cs-desafio-node

![AirBnb code style](https://img.shields.io/badge/code%20style-airbnb-blue.svg)

## Installation
```sh
  yarn
```

## Run
```sh
  yarn watch
```

## Test
```sh
  yarn test
```