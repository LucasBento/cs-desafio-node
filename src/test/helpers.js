import mongoose from 'mongoose';

import server from '../server';

const config = {
  db: {
    test: 'mongodb://localhost/db-test',
  },
  connection: null,
};

export const connect = () =>
  new Promise((resolve, reject) => {
    if (config.connection) {
      return resolve();
    }

    mongoose.Promise = Promise;

    const options = {
      server: {
        auto_reconnect: true,
        reconnectTries: Number.MAX_VALUE,
        reconnectInterval: 1000,
      },
    };

    mongoose.connect(config.db.test, options);

    config.connection = mongoose.connection;

    config.connection
      .once('open', resolve)
      .on('error', (e) => {
        if (e.message.code === 'ETIMEDOUT') {
          console.log(e);

          mongoose.connect(config.db.test, options);
        }

        console.log(e);
        reject(e);
      });
  });

function clearDatabase() {
  return new Promise((resolve) => {
    Object.keys(mongoose.connection.collections).forEach(key =>
      mongoose.connection.collections[key].remove(() => {}));

    resolve();
  });
}

export const setupTest = async () => {
  const app = await server.listen();

  await connect();
  await clearDatabase();

  return app;
};
