import mongoose from 'mongoose';

export default new mongoose.Schema({
  number: {
    type: Number,
    required: true,
  },
  ddd: {
    type: Number,
    required: true,
    default: 11,
  },
});
