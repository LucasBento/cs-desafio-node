import 'babel-polyfill';

import connectDatabase from './config/database';
import server from './server';

const port = process.env.PORT || 5000;

(async () => {
  try {
    await connectDatabase();
  } catch (error) {
    console.error('Unable to connect to database');

    // Exit Process if there is no database connection
    process.exit(1);
  }

  await server.listen(port);

  console.log(`Server started on http://localhost:${port}/`);
})();
