import compose from 'koa-compose';
import Router from 'koa-router';

import user from './routes/user';

export default () => {
  const router = new Router();

  user(router);

  return compose([
    router.routes(),
    router.allowedMethods(),
  ]);
};
