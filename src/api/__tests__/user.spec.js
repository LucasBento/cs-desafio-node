import supertest from 'supertest';
import jwt from 'jsonwebtoken';

import { setupTest } from '../../test/helpers';
import { User } from '../../model';

let request = null;
beforeEach(async () => {
  const app = await setupTest();

  request = supertest(app);
});

describe('User', () => {
  const user = {
    name: 'UsuarioExemplo',
    email: 'email@exemplo.com',
    password: 'senha',
    telephones: [{
      number: '123456789',
      ddd: '21',
    }],
  };

  describe('POST /user/signUp', () => {
    it('should return new user with token', async () => {
      await request.post('/user/signUp')
        .send(user)
        .expect((res) => {
          res.body = {
            id: !!res.body.id,
            createdAt: !!res.body.createdAt,
            updatedAt: !!res.body.updatedAt,
            token: !!res.body.token,
          };
        })
        .expect(200, {
          id: true,
          createdAt: true,
          updatedAt: true,
          token: true,
        });
    });

    it('should not return new user if it already exists', async () => {
      await new User(user).save();

      await request.post('/user/signUp')
        .send(user)
        .expect(409, {
          message: 'Usuário já existe!',
        });
    });
  });

  describe('POST /user/signIn', () => {
    it('should sign in with valid user', async () => {
      await new User(user).save();

      await request.post('/user/signIn')
        .send({
          email: user.email,
          password: user.password,
        })
        .expect((res) => {
          res.body = {
            id: !!res.body.id,
            createdAt: !!res.body.createdAt,
            updatedAt: !!res.body.updatedAt,
            token: !!res.body.token,
          };
        })
        .expect(200, {
          id: true,
          createdAt: true,
          updatedAt: true,
          token: true,
        });
    });

    it('should not sign in with invalid password', async () => {
      await new User(user).save();

      await request.post('/user/signIn')
        .send({
          email: user.email,
          password: `INVALID${user.password}`,
        })
        .expect(401, {
          message: 'Usuário e/ou senha inválido(s)',
        });
    });
  });

  describe('GET /user/search/:name', () => {
    it('should return users', async () => {
      const newUser = await new User(user).save();

      const token = jwt.sign({
        id: newUser._id,
      }, process.env.JWT_SECRET, {
        expiresIn: '3h',
      });

      await request.get('/user/search/UsuarioExemplo')
        .set('authentication', `Bearer ${token}`)
        .expect((res) => {
          res.body.users = res.body.users.map(resUser => ({
            ...resUser,
            _id: !!resUser._id,
            password: !!resUser.password,
          }));
        })
        .expect(200, {
          users: [{
            ...user,
            _id: true,
            password: false,
          }],
        });
    });
  });
});
