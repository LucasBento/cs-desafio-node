import jwt from 'jsonwebtoken';

import { User } from '../model';

export default async (ctx, next) => {
  const { headers } = ctx.request;

  // Check if authentication header exists
  const authentication = headers.authentication || headers.authentication;
  if (!authentication || authentication.indexOf('Bearer') === -1) {
    ctx.status = 401;
    ctx.body = {
      mensagem: 'Não autorizado',
    };
    return;
  }

  let token = null;
  try {
    token = jwt.verify(authentication.replace('Bearer', '').trim(), process.env.JWT_SECRET);
  } catch ({ name }) {
    let mensagem = 'Não autorizado';
    if (name === 'TokenExpiredError') {
      mensagem = 'Sessão inválida';
    }

    ctx.status = 401;
    ctx.body = {
      mensagem,
    };

    return null;
  }

  const user = await User.findOne({
    _id: token.id,
  });

  if (!user) {
    ctx.status = 401;
    ctx.body = {
      mensagem: 'Não autorizado',
    };
    return;
  }

  await next();
};
