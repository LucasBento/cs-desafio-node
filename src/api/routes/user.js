import jwt from 'jsonwebtoken';

import isAuthenticated from '../auth';
import { User } from '../../model';

export default (router) => {
  router
    // User sign up
    .post('/user/signUp', async (ctx) => {
      const {
        name,
        email,
        password,
        telephones,
      } = ctx.request.body;

      const user = await User.count({
        email,
      });

      if (user > 0) {
        ctx.status = 409;
        ctx.body = {
          message: 'Usuário já existe!',
        };

        return null;
      }

      const {
        _id: id,
        createdAt,
        updatedAt,
        lastLoginAt,
      } = await new User({
        name,
        email,
        password,
        telephones,
      }).save();

      const token = jwt.sign({
        id: user._id,
      }, process.env.JWT_SECRET, {
        expiresIn: '3h',
      });

      ctx.status = 200;
      ctx.body = {
        id,
        createdAt,
        updatedAt,
        lastLoginAt,
        token,
      };
    })

    // User sign in
    .post('/user/signIn', async (ctx) => {
      const { email, password } = ctx.request.body;

      const user = await User.findOne({
        email,
      });

      if (!user) {
        ctx.status = 401;
        ctx.body = {
          message: 'Usuário e/ou senha inválido(s)',
        };

        return null;
      }

      const isPasswordValid = await user.authenticate(password);
      if (!isPasswordValid) {
        ctx.status = 401;
        ctx.body = {
          message: 'Usuário e/ou senha inválido(s)',
        };

        return null;
      }

      const {
        _id: id,
        createdAt,
        updatedAt,
        lastLoginAt,
      } = user;

      await user.update({
        lastLoginAt: new Date(),
      });

      const token = jwt.sign({
        id,
      }, process.env.JWT_SECRET, { expiresIn: '3h' });

      ctx.status = 200;
      ctx.body = {
        id,
        createdAt,
        updatedAt,
        lastLoginAt,
        token,
      };
    })

    // User search
    .get('/user/search/:name', isAuthenticated, async (ctx) => {
      const { name } = ctx.params;

      if (!name) {
        ctx.status = 400;
        ctx.body = {
          message: 'Nome inválido',
        };
      }

      const users = await User.find({
        name: new RegExp(`^${name}$`, 'i'),
      })
        .select('name email telephones.number telephones.ddd');

      ctx.status = 200;
      ctx.body = {
        users,
      };
    });
};
