import mongoose from 'mongoose';

export default () => {
  const database = process.env.NODE_ENV === 'production' ?
    process.env.DATABASE_PROD :
    process.env.DATABASE_LOCAL;

  return new Promise((resolve, reject) => {
    mongoose.Promise = global.Promise;

    mongoose.connection
      // Reject promise if an error occurred when trying to connect to MongoDB
      .on('error', (error) => {
        console.log('Connection to database failed');

        reject(error);
      })

      // Exit process if there is no longer a database connection
      .on('close', () => {
        console.log('Lost connection to database');

        process.exit(1);
      })

      // Connected to database
      .once('open', () => {
        // Display connection information
        const { connections } = mongoose;

        connections.map(connection =>
          console.log(`Connected to ${connection.host}:${connection.port}/${connection.name}`),
        );

        resolve();
      });

    mongoose.connect(database);
  });
};
