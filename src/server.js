import Koa from 'koa';
import dotenvSafe from 'dotenv-safe';

import middleware from './middleware';

// Load the environment variables from `.env` file
dotenvSafe.load();

import api from './api'; // eslint-disable-line import/first

const app = new Koa();

app.keys = process.env.JWT_SECRET;

app.use(middleware());
app.use(api());
app.use((ctx) => {
  ctx.status = 404;
});

export default app;
